#include<iostream>
#include<queue>

class SpaceBTree {
public:
struct __n {
	char c;
	int current;
	std::vector<std::unique_ptr<__n>> next; // list of child node
};
private:

	int _b_print(__n *tree, int is_left, int start, int depth, char s[20][255])
	{
		char b[20];
		int height = 3;
		int width = 4;

		sprintf(b, "[%c%d]", tree->c, tree->current);

		int endpoint = start;
		for (int i = 0; i < tree->next.size(); i++) {
			endpoint = _b_print(tree->next[i].get(), 1, endpoint, depth + 1, s);
		}

		//printf("%d, %d, %d\n", depth, start, endpoint);

		for (int i = 0, k = 0; k < tree->next.size() && i < endpoint - start; i++) {
			if (s[height * depth + height - 1][start + i] == '+')
				k++;
			else if (k)
				s[height * depth + height - 1][start + i] = '-';
		}

		for (int i = 0; i < width; i++)
			s[height * depth][(start + endpoint) / 2 + i] = b[i];
		if (tree->next.size()>0)
			s[height * depth + 1][(start + endpoint + width) / 2] = '|';
		if (depth)
			s[height * depth - 1][(start + endpoint + width) / 2] = '+';

		return endpoint + width;
	}

	std::unique_ptr<__n> root;
public:
	SpaceBTree() {
		root = std::make_unique<__n>();
		root->c = ' ';
		root->current = -1;
	}

	void printtree()
	{
		char s[30][255];
		for (int i = 0; i < 30; i++)
			sprintf(s[i], "%250c", '\0');

		_b_print(root.get(), 0, 0, 0, s);

		for (int i = 0; i < 30; i++)
			printf("%s\n", s[i]);
	}
	
	__n* getRoot() {
		return root.get();
	}

	__n* addNode(__n* p, char c, int current) {
		p->next.push_back(std::make_unique<__n>());
		__n* newnode = p->next.back().get();
		newnode->c = c;
		newnode->current = current;
		return newnode;
	}

};

class nQueens {
private:
	int size;
	int *col;
	int ans = 0;
	SpaceBTree st;
	SpaceBTree::__n* root = st.getRoot();

	bool promising(int i) {
		for (int x = 0; x < i; x++) {
			if (col[i] == col[x] || (abs(col[i] - col[x]) == abs(i-x))) return false;
		}
		return true;
	}
public:
	nQueens(int size) {
		this->size = size;
		col = new int[size];
	}

	void solve(int i, SpaceBTree::__n* p = NULL) {
		if (ans == 2) return;
		if (p == NULL)
			p = root;
		if (i == size-1) {
			ans++;
			std::cout << "found! :\n";
			for(int x = 0; x < size; x++) {
				printf("(%d, %d)", x, col[x]);
			}
			std::cout << "\n";
			return;
		}
		for (int x = 0; x < size; x++) {
			col[i + 1] = x;
			if (promising(i+1)) {
				auto nextp = st.addNode(p, 'v', x);
				solve(i+1, nextp);
			}
			else {
				auto nextp = st.addNode(p, 'x', x);
			}
		}
	}

	void printtree() {
		st.printtree();
	}

	~nQueens() {
		delete[] col;
	}
};

class nColoring {
private:
	int count;
	int colors;
	const bool *adj; // count*count
	int *nodes; // colors in node
	SpaceBTree st;
	SpaceBTree::__n* root = st.getRoot();
	bool promising(int i) {
		for (int x = 0; x < count; x++) {
			if (adj[i*count + x] && nodes[i] == nodes[x]) {
				return false;
			}
		}
		return true;
	}
public:
	nColoring(int nodecount, int numcolors, const bool *adjmatrix) {
		count = nodecount;
		colors = numcolors;
		adj = adjmatrix;
		nodes = new int[count];
		for (int i = 0; i < count; i++) {
			nodes[i] = -1;
		}
	}

	void solve(int i, SpaceBTree::__n* p = NULL) {
		if (p == NULL)
			p = root;
		if (i == count - 1) {
			std::cout << "found! :\n";
			for (int x = 0; x < count; x++) {
				printf("(%d, %d)", x, nodes[x]);
			}
			std::cout << "\n";
			return;
		}
		for (int x = 0; x < colors; x++) {
			nodes[i + 1] = x;
			if (promising(i + 1)) {
				auto nextp = st.addNode(p, 'v', x);
				st.printtree();
				solve(i + 1, nextp);
			}
			else {
				auto nextp = st.addNode(p, 'x', x);
			}
			nodes[i + 1] = -1;
		}
	}

	void printtree() {
		st.printtree();
	}

	~nColoring() {
		delete[] nodes;
	}
};

class ZeroOneKnapsack {
private:
	struct __n {
		int depth;
		int profit;
		int weight;
		int bound;
		__n *pick;
		__n *unpick;
	};

	int maxprofit;
	int count;
	int maxweight;
	const int (*w)[3];
	std::queue<__n*> q;
	__n* root = NULL;
	void __cascade__del__(__n* n) {
		__n *l, *r;
		if (n == NULL) return;
		l = n->pick;
		r = n->unpick;
		delete n;
		__cascade__del__(l);
		__cascade__del__(r);
	}
	int calcbound(__n* n) {
		int bound = n->profit;
		int weight = n->weight;
		if (maxweight < weight)
			return 0;
		int i;
		for (i = n->depth; i < count; i++) {
			weight += w[i][1];
			if (weight > maxweight) break;
			bound += w[i][0];
		}
		if(i<count)
			bound += (maxweight - weight + w[i][1]) * w[i][2];
		return bound;
	}

	int _print_t(__n *tree, int is_left, int offset, int depth, char s[20][255])
	{
		char b[3][20];
		int width = 5;

		if (!tree) return 0;

		sprintf(b[0], "(%3d)", tree->profit);
		sprintf(b[1], "(%3d)", tree->weight);
		sprintf(b[2], "(%3d)", tree->bound);

		int left = _print_t(tree->pick, 1, offset, depth + 1, s);
		int right = _print_t(tree->unpick, 0, offset + left + width, depth + 1, s);

#ifdef COMPACT
		for (int i = 0; i < width; i++)
			s[depth][offset + left + i] = b[i];

		if (depth && is_left) {

			for (int i = 0; i < width + right; i++)
				s[depth - 1][offset + left + width / 2 + i] = '-';

			s[depth - 1][offset + left + width / 2] = '.';

		}
		else if (depth && !is_left) {

			for (int i = 0; i < left + width; i++)
				s[depth - 1][offset - width / 2 + i] = '-';

			s[depth - 1][offset + left + width / 2] = '.';
		}
#else	
		for(int x=0; x<3;x++)
			for (int i = 0; i < width; i++)
					s[4 * depth + x][offset + left + i] = b[x][i];

		if (depth && is_left) {

			for (int i = 0; i < width + right; i++)
				s[4 * depth - 1][offset + left + width / 2 + i] = '-';

			s[4 * depth - 1][offset + left + width / 2] = '+';
			s[4 * depth - 1][offset + left + width + right + width / 2] = '+';

		}
		else if (depth && !is_left) {

			for (int i = 0; i < left + width; i++)
				s[4 * depth - 1][offset - width / 2 + i] = '-';

			s[4 * depth - 1][offset + left + width / 2] = '+';
			s[4 * depth - 1][offset - width / 2 - 1] = '+';
		}
#endif

		return left + width + right;
	}

public:
	ZeroOneKnapsack(int n, int max_weight, const int weight_table[][3]) {
		count = n;
		maxweight = max_weight;
		w = weight_table;
	}
	
	void solve() {
		if (root != NULL) {
			return;
		}
		__n *v = new __n();
		root = v;
		v->bound = calcbound(v);
		q.push(v);
		maxprofit = 0;
		while (!q.empty()) {
			v = q.front();
			q.pop();
			//printf("mp: %d, profit: %d, bound: %d, weight: %d\n", maxprofit, v->profit, v->bound, v->weight);

			if (v->weight > maxweight || v->bound <= maxprofit) continue;
			if (v->depth != count) {
				if (v->profit > maxprofit) maxprofit = v->profit;
				for(int i=0;i<2;i++){
					__n *u = new __n();
					u->depth = v->depth + 1;
					if (i == 0) {
						u->profit = v->profit + w[v->depth][0];
						u->weight = v->weight + w[v->depth][1];
						v->pick = u;
					}
					else {
						u->profit = v->profit;
						u->weight = v->weight;
						v->unpick = u;
					}
					u->bound = calcbound(u);
					if (u->bound > maxprofit) 
						q.push(u);
					printf("mp: %d, profit: %d, bound: %d, weight: %d\n", maxprofit, u->profit, u->bound, u->weight);
				}
			}
		}
	}

	void printtree()
	{
		char s[30][255];
		for (int i = 0; i < 30; i++)
			sprintf(s[i], "%150c", '\0');

		_print_t(root, 0, 0, 0, s);

		for (int i = 0; i < 30; i++)
			printf("%s\n", s[i]);
	}
	
	~ZeroOneKnapsack() {
		__cascade__del__(root);
	}
};

class TravelingSalesman {
private:
	struct __n {
		int depth;
		int dist;
		std::unique_ptr<int[]> visited;
		int bound;
		std::vector<__n*> next; // list of child node
	};
	int count;
	int *w; // n x n;
	int minimum;
	__n* root;
	class Compare {
	public:
		bool operator() (__n* a, __n* b) {
			return a->bound > b->bound;
		}
	};
	std::priority_queue < __n*, std::vector<__n*>, Compare > q;

	bool isvisited(int vi, std::unique_ptr<int[]>& visited, int size) {
		for (int i = 1; i < size; i++) {
			if (visited[i] == vi) {
				return true;
			}
		}
		return false;
	}

	void vcopy(__n& p, __n& c) {
		for (int i = 0; i <= p.depth; i++) {
			c.visited[i] = p.visited[i];
		}
	}


	int _b_print(__n *tree, int is_left, int start,  int depth, char s[20][255])
	{
		char b[3][20];
		int width = 5;

		sprintf(b[0], "(%3d)", tree->visited[depth]);
		sprintf(b[1], "(%3d)", tree->dist);
		sprintf(b[2], "(%3d)", tree->bound);

		int endpoint = start;
		for (unsigned int i = 0; i < tree->next.size(); i++) {
			endpoint = _b_print(tree->next[i], 1, endpoint, depth + 1, s);
		}

		//printf("%d, %d, %d\n", depth, start, endpoint);

		for (unsigned int i = 0, k = 0; k < tree->next.size() && i < endpoint-start; i++) {
			if (s[5 * depth + 4][start + i] == '+')
				k++;
			else if(k)
				s[5 * depth + 4][start + i] = '-';
		}

		for (int x = 0; x<3; x++)
			for (int i = 0; i < width; i++)
				s[5 * depth + x][(start + endpoint) / 2 + i] = b[x][i];
		if (tree->next.size()>0)
				s[5 * depth + 3][(start + endpoint + width) / 2] = '|';
		if(depth)
			s[5 * depth - 1][(start + endpoint + width) / 2] = '+';

		return endpoint + width;
	}

	int calcbound(__n& node, std::unique_ptr<int[]>& va) {
		int vlen = node.depth;
		int bound = node.dist;
		int min = 0x7FFFFFFF;
		int s = node.visited[vlen];
		
		for (int i = 1; i < count - vlen; i++) { //from s
			int vmin = w[s*count + va[i]];
			if ( min > vmin) {
				min = vmin;
			}
		}
		bound += min;
		//printf("%d\n", min);
		for (int x = 1; x < count - vlen; x++) {
			min = 0x7FFFFFFF;
			for (int y = 0; y < count - vlen; y++) {
				int vmin = w[va[x]*count + va[y]];
				if (vmin > 0 && min > vmin) {
					min = vmin;
				}
			}
			bound += min;
			//printf("%d\n", min);
		}
		return bound;
	}

public:
	TravelingSalesman(int count, int *w) {
		this->count = count;
		this->w = w;
	}
	void solve() {
		if (root != NULL) {
			return;
		}
		__n *v = new __n();
		root = v;

		auto va = std::make_unique<int[]>(count); // {1} + V - A(1...k)
		for (int i = 0; i < count; i++) {
			va[i] = i;
		}

		v->visited = std::make_unique<int[]>(1);
		v->visited[0] = 0;
		v->bound = calcbound(*v, va);

		q.push(v);
		minimum = 0xFFFF;

		while (!q.empty()) {
			v = q.top();
			q.pop();
			printf("visited: m: %d, depth: %d, dist: %d, bound: %d\n", minimum, v->depth, v->dist, v->bound);

			if (v->bound >= minimum) continue;
			va = std::make_unique<int[]>(count - v->depth); // {1} + V - A(1...k)

			va[0] = 0;

			for (int i = 1, vi = 1; i < count - v->depth;) {
				if (!isvisited(vi, v->visited, v->depth+1)) {
					va[i] = vi;
					i++;
				}
				vi++;
			}
			if (v->depth != count - 2) {
				for (int i = 1; i<count-v->depth; i++) {
					__n *u = new __n();
					int s = v->visited[v->depth];
					u->depth = v->depth + 1;
					u->dist = v->dist + w[s*count + va[i]];
					u->visited = std::make_unique<int[]>(u->depth + 1);
					vcopy(*v, *u);
					u->visited[u->depth] = va[i];
					v->next.push_back(u);
					auto vb = std::make_unique<int[]>(count - u->depth); // V - A(1...k+va[i])
					for (int x = 0, y = 0; x < count - v->depth; x++) {
						if (x == i) continue;
						vb[y] = va[x];
						y++;
					}
					u->bound = calcbound(*u, vb);
					if (u->bound < minimum)
						q.push(u);
					printf("m: %d, depth: %d, dist: %d, bound: %d\n", minimum, u->depth, u->dist, u->bound);
				}
			}
			else {
				printf("[");
				for (int i = 0; i <= v->depth; i++) {
					printf("%d, ", v->visited[i]);
				}
				printf("%d, ", va[1]);
				printf("%d, ", va[0]);
				v->dist += w[v->visited[v->depth] * count + va[1]] + w[va[1] * count + va[0]];
				printf("] dist: %d \n", v->dist);
				if (v->dist < minimum) minimum = v->dist;
			}
		}
	}
	void printtree()
	{
		char s[30][255];
		for (int i = 0; i < 30; i++)
			sprintf(s[i], "%150c", '\0');

		_b_print(root, 0, 0, 0, s);

		for (int i = 0; i < 30; i++)
			printf("%s\n", s[i]);
	}
};

int main() {
	nQueens nq(5);
	nq.solve(-1);
	nq.printtree();
	bool w[] = {
		0, 1, 1, 1,
		1, 0, 1, 0,
		1, 1, 0, 1,
		1, 0, 1, 0
	};
	nColoring nc(4, 3, w);
	nc.solve(-1);
	//nc.printtree();
	/*
	int wt[][3] = {
		{ 40, 2, 20 },
		{ 30, 5, 6 },
		{ 50, 10, 5 },
		{ 10, 5, 2 },
	};
	ZeroOneKnapsack znk(4, 16, wt);
	znk.solve();
	znk.printtree();
	int wt2[][3] = {
		{ 20, 2, 10 },
		{ 30, 5, 6 },
		{ 35, 7, 5 },
		{ 12, 3, 4 },
		{ 3, 1, 3 },
	};
	ZeroOneKnapsack znk2(5, 13, wt2);
	znk2.solve();
	znk2.printtree();
	int tw[] = {
		0, 14, 4, 10, 20,
		14, 0, 7, 8, 7,
		4, 5, 0, 7, 16,
		11, 7, 9, 0, 2,
		18, 7, 17, 4, 0
	};
	TravelingSalesman tsp(5, tw);
	tsp.solve();
	tsp.printtree();
	int tw2[] = {
		0, 5, 8, 0xFFFF, 0xFFFF, 0xFFFF,
		0xFFFF, 0, 4, 4, 0xFFFF, 0xFFFF,
		0xFFFF, 0xFFFF, 0, 0xFFFF, 0xFFFF, 5,
		1, 0xFFFF, 0xFFFF, 0, 0xFFFF, 0xFFFF,
		0xFFFF, 6, 0xFFFF, 2, 0, 0xFFFF,
		0xFFFF, 0xFFFF, 0xFFFF, 5, 8, 0,

	};
	TravelingSalesman tsp2(6, tw2);
	tsp2.solve();
	tsp2.printtree();*/
	getchar();
}